const output = document.getElementById('output');

const mergeSort = (array) => {
  if (array.length <= 1) {
    return array;
  }

  let mid = Math.floor(array.length / 2);

  let left = array.slice(0, mid);
  let right = array.slice(mid, array.length);

  return merge(
    mergeSort(left),
    mergeSort(right)
  );
}

const merge = (left, right) => {
  let output = [];

  let l = 0;
  let r = 0;

  while (l < left.length && r < right.length) {
    let lValue = left[l];
    let rValue = right[r];

    if (lValue < rValue) {
      output.push(lValue);
      l++;
    } else {
      output.push(rValue);
      r++;
    }
  }

  return [...output, ...left.slice(l), ...right.slice(r)];
}

output.innerText = mergeSort(array = [1, 4, 2, 345, 123, 43, 32, 5643, 63, 123, 43, 2, 55, 1, 234, 92, 3]);
